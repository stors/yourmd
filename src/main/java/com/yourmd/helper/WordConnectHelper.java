package com.yourmd.helper;

import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;
import com.hazelcast.nio.serialization.DataSerializable;

import java.io.IOException;

/**
 * Hazelcast value for storing connected string with window size.
 * Create custom serialization for much better/smaller storage
 * Created by stors on 21/01/2017.
 */
public class WordConnectHelper implements DataSerializable {

    private String connect;
    private int windowSize;

    /**
     * Default constructor must always exists when serializable is present
     */
    public WordConnectHelper() {
    }

    public WordConnectHelper(String connect, int windowSize) {
        this.connect = connect;
        this.windowSize = windowSize;
    }

    public String getConnect() {
        return connect;
    }

    public void setConnect(String connect) {
        this.connect = connect;
    }

    public int getWindowSize() {
        return windowSize;
    }

    public void setWindowSize(int windowSize) {
        this.windowSize = windowSize;
    }

    @Override
    public void writeData(ObjectDataOutput out) throws IOException {
        out.writeUTF(connect);
        out.writeInt(windowSize);
    }

    @Override
    public void readData(ObjectDataInput in) throws IOException {
        this.connect = in.readUTF();
        this.windowSize = in.readInt();
    }

    @Override
    public String toString() {
        return "WordConnectHelper{" +
                "connect='" + connect + '\'' +
                ", windowSize=" + windowSize +
                '}';
    }
}
