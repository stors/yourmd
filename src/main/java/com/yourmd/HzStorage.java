package com.yourmd;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.yourmd.helper.WordConnectHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

/**
 * Initial storage words/phrases to storage.
 * Created by stors on 20/01/2017.
 */
@Component
public class HzStorage {

    private static final Logger LOG = LoggerFactory.getLogger(HzStorage.class);

    private static final String FILE_NAME = "phrases";
    private static final int START_LINE_ID = 100000;
    private static final String LINE_SPLITTER = " |,|;|:";

    @Autowired
    private HazelcastInstance hzInstance;

    @PostConstruct
    public void init() {
        LOG.debug("Fill  Hazelcast database");
        try {
            readAndFillData();
        } catch (IOException e) {
            LOG.error("Something when wrong when filling the data", e);
        }
    }

    public IMap<String, WordConnectHelper> wordMap() {
        return getHzInstance().getMap(Application.WORD_MAP);
    }

    public IMap<String, String> wordConnectMap() {
        return getHzInstance().getMap(Application.PHRASES_MAP);
    }

    public HazelcastInstance getHzInstance() {
        return hzInstance;
    }

    /**
     * Read from provided file and store it to hazelcast
     * @throws IOException file could not be read
     */
    private void readAndFillData() throws IOException {
        String line;
        int lineId = START_LINE_ID;
        final InputStream is = HzStorage.class.getClassLoader().getResourceAsStream(FILE_NAME);

        try (BufferedReader br = new BufferedReader(new InputStreamReader(is, StandardCharsets.UTF_8))) {
            while ((line = br.readLine()) != null) {
                lineId = storeOneLineOfPhrases(line.split(LINE_SPLITTER), lineId++);
            }
        }
    }

    /**
     * Store unique words with generated ID and window size (wordMap)
     * Connect each phrases with generated IDs (unique ID for the word) and restore back the original phrases (wordConnectMap)
     * Window size is updated only for first word in one line phrase.
     * First word in phrase has window size to lookup ahead if phrase exists.
     * Word is unique identified as number. Phrase can be build using numbers.
     * @param linePhrase one line break down to words
     * @param lineId counter unique
     * @return line ID
     */
    private int storeOneLineOfPhrases(String[] linePhrase, int lineId) {
        IMap<String, String> wordConnector = wordConnectMap();
        IMap<String, WordConnectHelper> word = wordMap();
        StringBuilder buildKey = new StringBuilder();
        StringBuilder buildPhrases = new StringBuilder();
        for (int i = 0; i < linePhrase.length; i++) {
            final String phrase = linePhrase[i].trim().toLowerCase();
            if (!phrase.isEmpty()) {
                String valFromLineId = Integer.toString(lineId++, Character.MAX_RADIX);
                WordConnectHelper alreadyExists = word.putIfAbsent(phrase, new WordConnectHelper(valFromLineId,
                        (i == 0) ? linePhrase.length : 0));
                if (alreadyExists != null) {
                    valFromLineId = alreadyExists.getConnect();
                    if(i == 0) {
                        word.replace(phrase, new WordConnectHelper(valFromLineId,
                                Math.max(linePhrase.length, alreadyExists.getWindowSize())));
                    }
                }
                buildKey.append(valFromLineId);
                buildPhrases.append(phrase).append(" ");
            }
        }

        wordConnector.put(buildKey.toString(), buildPhrases.toString().trim());
        return lineId;
    }

    @PreDestroy
    public void destroy() {
        if (hzInstance != null)
            hzInstance.shutdown();
        LOG.info("===> HzInstance - call DESTROY..");
    }

}
