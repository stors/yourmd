package com.yourmd.validator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * Validator for controller
 * Created by stors on 22/01/2017.
 */
@Component
public class InputValidator {

    private static final Logger LOG = LoggerFactory.getLogger(InputValidator.class);

    public boolean validateForEmptiness(String text) {
        LOG.debug("Validate string if empty, {}", text);
        return !(text == null || text.trim().isEmpty());
    }
}
