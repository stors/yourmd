package com.yourmd.controller;

import com.yourmd.service.SearchService;
import com.yourmd.validator.InputValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@RestController
public class SearchController {

    private static final Logger LOG = LoggerFactory.getLogger(SearchController.class);
    private static final String IN_TEXT_SPLITTER = " |,|;|:|\\.";
    public static final String SEARCH_TXT_VALIDATOR_ERROR = "Input string can not be empty!";

    @Autowired
    private SearchService searchService;
    @Autowired
    private InputValidator validator;

    /**
     * Match input text from provided text stored phrases.
     * Also add validation to validate input text.
     * @param inText input text
     * @return phrases from input text
     */
    @RequestMapping(value = "/", method = RequestMethod.POST)
    public ResponseEntity<Set<String>> searchAndMatch(@RequestBody String inText) {

        LOG.debug("searchAndMatch={}", inText);
        if(!validator.validateForEmptiness(inText)) {
            return new ResponseEntity<>(new HashSet<String>()
                {{add(SEARCH_TXT_VALIDATOR_ERROR);}}, HttpStatus.BAD_REQUEST);
        }
        long start = System.currentTimeMillis();
        List<String> splitPhrases = Arrays.asList(inText.split(IN_TEXT_SPLITTER));
        Set<String> result = searchService.searchForMatchedPharses(splitPhrases);
        LOG.debug("Search for the phrases: {}ms", (System.currentTimeMillis() - start));
        return new ResponseEntity<>(result, HttpStatus.OK);
    }
}
