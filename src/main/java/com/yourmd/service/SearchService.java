package com.yourmd.service;

import com.yourmd.data.StorageData;
import com.yourmd.helper.WordConnectHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Logic/service methods
 * Created by stors on 21/01/2017.
 */
@Service
public class SearchService {

    private static final Logger LOG = LoggerFactory.getLogger(SearchService.class);

    @Autowired
    private StorageData storage;

    /**
     * First identify first word in phrase
     * If word exists we provide window size.
     * Window size looks ahead of current word if phrase matches stored phrase.
     * When searching with window, words are changed to number - lookup if provided number matched phrase.
     * @param inPhrases already break down to list
     * @return phrases found
     */
    public Set<String> searchForMatchedPharses(List<String> inPhrases) {
        LOG.debug("Try to match phrases from {}", inPhrases);
        final int inPhrasesLen = inPhrases.size();
        Set<String> results = new HashSet<>();
        for (int i = 0; i < inPhrases.size(); i++) {
            if(!inPhrases.get(i).isEmpty()) {
                final WordConnectHelper findMatch = storage.findWordMatch(inPhrases.get(i));
                if(findMatch != null) {
                    final int windowSize = findMatch.getWindowSize();
                    final int windowAhead = ((i + windowSize) > inPhrasesLen) ? inPhrasesLen : (i + windowSize);
                    final List<String> lookAhead = inPhrases.subList(i, windowAhead);
                    if(!lookAhead.isEmpty())
                        results.addAll(lookForMatches(lookAhead));
                }
            }
        }
        return results;
    }

    /**
     * @param lookAheadList search from look ahead list provided from window parameter
     * @return sub list phrases found
     */
    private Set<String> lookForMatches(List<String> lookAheadList) {
        final StringBuilder connector = new StringBuilder();
        return lookAheadList.stream()
                .map(storage::findWordMatch)
                .filter(wordMatch -> wordMatch != null)
                .map(wordMatchExists -> storage.findWordConnectMatch(
                        connector.append(wordMatchExists.getConnect()).toString()))
                .filter(wordConnectMatch -> wordConnectMatch != null)
                .collect(Collectors.toSet());
    }
}
