package com.yourmd.data;

import com.hazelcast.core.IMap;
import com.yourmd.HzStorage;
import com.yourmd.helper.WordConnectHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Provides storage helper class. Lookup to database for key.
 * Created by stors on 21/01/2017.
 */
@Repository
public class StorageData {

    private IMap<String, WordConnectHelper> wordMap;
    private IMap<String, String> wordConnectMap;

    @Autowired
    private void setWordMap(HzStorage hzStorage) {
        this.wordMap = hzStorage.wordMap();
    }

    @Autowired
    private void setWordConnectMap(HzStorage hzStorage) {
            this.wordConnectMap = hzStorage.wordConnectMap();
    }

    /**
     * Find unique identified word from storage
     * @param word lookup for word
     * @return match the input word
     */
    public WordConnectHelper findWordMatch(String word) {
        return wordMap.get(word.toLowerCase());
    }

    /**
     * Find if words are connected to riht phrases
     * @param connectId word to number lookup
     * @return match phrase
     */
    public String findWordConnectMatch(String connectId) {
        return wordConnectMap.get(connectId);
    }
}
