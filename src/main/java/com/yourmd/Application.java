package com.yourmd;

import com.hazelcast.config.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class Application {

    private static final Logger LOG = LoggerFactory.getLogger(Application.class);

    public static final String WORD_MAP = "wordMap";
    public static final String PHRASES_MAP = "phrasesMap";

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    /**
     * Hazelcast map configuration - to know how
     * @return hazelcat map config
     */
    @Bean
    public Config hzConfig() {
        LOG.debug("Create Hazelcast config");
        Config hzConf = new Config();

        hzConf.setInstanceName("YourMD-Stor");

        // disable Hz management center
        hzConf.getManagementCenterConfig().setEnabled(false);

        // create username and password for Hz storage
        GroupConfig groupConfig = hzConf.getGroupConfig();
        groupConfig.setName("yourmd");
        groupConfig.setPassword("yourmd");

        NetworkConfig network = hzConf.getNetworkConfig();
        network.setPort(5710);
        network.setPortAutoIncrement(false);
        network.setReuseAddress(true);
        JoinConfig join = network.getJoin();
        join.getMulticastConfig().setEnabled(false);

        hzConf.addMapConfig(new MapConfig()
                .setName(WORD_MAP)
                .setBackupCount(0)
                .setTimeToLiveSeconds(0)
                .setEvictionPolicy(EvictionPolicy.NONE)
                .setInMemoryFormat(InMemoryFormat.BINARY));

        hzConf.addMapConfig(new MapConfig()
                .setName(PHRASES_MAP)
                .setBackupCount(0)
                .setTimeToLiveSeconds(0)
                .setEvictionPolicy(EvictionPolicy.NONE)
                .setInMemoryFormat(InMemoryFormat.BINARY));

        return hzConf;
    }
}
