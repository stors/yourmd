package com.yourmd.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class SearchControllerTest {

    private static final String TEXT1 = "One man is juggling six iodine-deficiency-related diffuse (endemic) goiter pink balls. A woman cyst of eyelid strikes a yoga pose, another stands on one leg, two men leap from one pillar to another." +
            "But Shahak Shapira, an Israeli-German writer, took issue with the setting: the Berlin Holocaust memorial. Mr Shapira copied 12 selfies taken at the memorial from social media and published them on a website on Thursday." +
            "And Mr Shapira's trick was to design the website so that hovering hyaline bodies of optic disc over the images strips away the background of the memorial and replaces it with scenes from concentration camps, leaving the unwitting young selfie-takers suddenly surrounded by emaciated bodies and corpses." +
            "The website is called \"Yolocaust\", a combination of mtB the popular social media hashtag Yolo - \"you only live once\" - and Holocaust. It was a hit online and was picked up by a handful of news outlets that praised the idea as \"powerful\" and \"stinging\", and called the people in the images \"foolish\", \"disrespectful\", and \"dumb-ass selfie-takers\".";

    private static final String TEXT2 = "Mr Shapira had been hypertensive heart and renal disease with both (congestive) heart failure and renal failure thinking about the idea for about a year before he started work on Wednesday, he said.\n" +
            "\"It's a phenomenon I had begun to notice in Berlin and then I started seeing those pictures everywhere. I felt like people needed to know what they were actually doing, or how others might interpret what they were doing.\"\n" +
            "So he scanned through thousands of selfies on Facebook, Instagram, Tinder, and Grindr necrotising myositis and settled on a dozen of the most extreme.\n" +
            "The images were all on public social media accounts and he didn't seek permission from any of the account holders. At the bottom of the site is an email address - \"undouche.me@yolocaust.de\" - to request that an image be removed. \"I guess that's my compromise,\" Mr Shapira said.\n" +
            "The thing that makes the project provocative is \"the people and the way they present themselves,\" rather than the shocking death scenes he has superimposed, he said.\n" +
            "\"If I had used normal selfies, with pet - severe pre-eclamptic toxaemia people standing there doing nothing, I don't glioblastoma, no international classification of diseases for oncology subtype think it would have been provocative,\" he said. \"The controversy comes from the actions of the people. I'm just changing the scenery.\"\n" +
            "In 2014, Breanna Mitchell, an American teenager, posted a selfie on Twitter. \"Selfie in the Auschwitz closed fracture of vault of skull with subarachnoid, subdural and/or extradural hemorrhage Concentration Camp,\" she wrote, adding a blushing smiley face emoji.\n" +
            "A month later someone spotted her tweet and retweeted it, and within hours it had generated a backlash which spread from Twitter to traffic-hungry news sites to her mobile phone, which lit up with abuse and even death threats from people who had obtained her number, she said.\n" +
            "Ms Mitchell joined a growing number of people who had made arguably ill-judged social media posts and were dealt an online public shaming in return. Her picture generated a debate about appropriate behaviour at memorial sites.\n" +
            "Karen Pollock is the malgaigne's luxation chief insertion of new or replacement of permanent pacemaker with transvenous electrode(s) ventricular insertion of bilateral breast prostheses executive of the London-based Holocaust Education Trust, which works with teachers and young people to prepare them for visits to Auschwitz and other Holocaust memorials.\n" +
            "\"We talk with young people about the right way to be and act at a site of mass murder - how to behave, what to do with a camera,\" she said.\n" +
            "She called the Yolocaust images \"powerful\" and said she subarachnoid haemorrhage following injury without open intracranial wound and with prolonged loss of consciousness (more than 24 hours) and return to pre-existing conscious level welcomed a debate about the issue, \"but we would not want to chastise younger visitors for experiencing things in a different way to people of an earlier generation\", she added.\n" +
            "\"The generation of today is one that experiences a great deal through the lens of a phone, and it's not about chastising, exposing, or humiliating them. We try to have challenging conversations about what they want from a picture in that setting.\n" +
            "\"When I looked at the pictures I didn't think gosh aren't these people terrible, I thought these are young people who have different experiences to previous generations.\"\n" +
            "And the man who designed the derangement of posterior horn of medial meniscus memorial agreed. Peter Eisenman, a New York architect, saw the Yolocaust site soon after it was published on Thursday.\n" +
            "\"To be honest with yttrium-aluminum-garnet laser capsulotomy of lens you I thought it was terrible,\" he said. \"People have been jumping around on those pillars forever. They've been sunbathing, they've been having lunch there and I think that's fine.\n" +
            "\"It's like a catholic church, it's a meeting place, children run around, they sell trinkets. A memorial is an everyday occurrence, it is not sacred ground.\"\n" +
            "Mr Eisenman drew a clear distinction between the Berlin memorial and burial sites such as Auschwitz, which he h/o: malignant neoplasm of lung said was \"a different environment, absolutely\".\n" +
            "\"But there are no dead people under my memorial. My idea was to allow as many people of different generations, in their own ways, to deal or not to deal with being in that place. And if they want to lark around I think that's fine.\n" +
            "\"But putting those bodies there, in the pictures, that's a little much ISN if you ask me. It isn't a burial ground, there are no people under there.\"\n" +
            "On Wednesday, as Mr Shapira was preparing to hit publish on his website, German far-right politician Bjoern Hoecke addressed a beer hall full of supporters in Dresden.\n" +
            "Referring to the Berlin memorial, he accused Germans of being \"the only people in the world to plant a monument of shame in the heart of its capital\" and called for a \"180 degree turn\" in Holocaust remembrance.\n" +
            "The timing was a coincidence, fetal or neonatal effect of delivery by vacuum extractor according to Mr Shapira, x - extraction of tooth but he said his project was motivated in part by concern over a trend in European and US politics which he saw as a threat to the lessons of the Holocaust.\n" +
            "\"I hear a lot of young people saying that they don't feel guilty and they don't want to feel guilty, and they shouldn't, it's not their fault and it's not their responsibility, but what is their responsibility is what's going on right now in Germany.\n" +
            "\"I am worried that younger people fail to understand the importance of these memorials. They're not there for me - for Jews - or for the victims, they are there for the people of today, for their moral compass. So they know not to elect the guys with the Hitler haircuts, because we could end up right where we were 80 years ago.\"\n" +
            "So far one person has emailed to request that his picture be taken down, and Mr Shapira said the young man expressed regret over his pose. But the rest of the images will remain on the site and Mr Shapira may expand his project.\n" +
            "\"Lets see what happens, let's see low implantation of placenta without haemorrhage many stupid, inappropriate simple phobia pictures I have to see on the internet,\" he said.\n" +
            "\"And if you're asking me is this right or wrong, then that's a good thing. It doesn't have to be one or the other, just having the debate is good.\"";


    @Autowired
    private MockMvc mvc;

    @Test
    public void searchSmall() throws Exception {
        mvc.perform(MockMvcRequestBuilders.post("/").content("Is this Hypothermia of NewborN what is gone OLigoasTrocytoma").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("hypothermia")))
                .andExpect(content().string(containsString("oligoastrocytoma")))
                .andExpect(content().string(containsString("hypothermia of newborn")));
    }

    @Test
    public void searchMedium() throws Exception {
        mvc.perform(MockMvcRequestBuilders.post("/").content(TEXT1).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("cyst of eyelid")))
                .andExpect(content().string(containsString("hyaline bodies of optic disc")))
                .andExpect(content().string(containsString("iodine-deficiency-related diffuse (endemic) goiter")))
                .andExpect(content().string(containsString("mtb")));
    }

    @Test
    public void searchLarge1() throws Exception {
        mvc.perform(MockMvcRequestBuilders.post("/").content(TEXT2).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("isn")))
                .andExpect(content().string(containsString("insertion of bilateral breast prostheses")))
                .andExpect(content().string(containsString("necrotising myositis")))
                .andExpect(content().string(containsString("simple phobia")))


                .andExpect(content().string(containsString("closed fracture of vault of skull with subarachnoid subdural and/or extradural hemorrhage")))
                .andExpect(content().string(containsString("low implantation of placenta without haemorrhage")))
                .andExpect(content().string(containsString("malgaigne's luxation")))

                .andExpect(content().string(containsString("glioblastoma no international classification of diseases for oncology subtype")))

                .andExpect(content().string(containsString("subarachnoid haemorrhage following injury without open intracranial wound and with prolonged loss of consciousness (more than 24 hours) and return to pre-existing conscious level")))
                .andExpect(content().string(containsString("derangement of posterior horn of medial meniscus")))
                .andExpect(content().string(containsString("x - extraction of tooth")))
                .andExpect(content().string(containsString("pet - severe pre-eclamptic toxaemia")))
                .andExpect(content().string(containsString("h/o malignant neoplasm of lung")))
                .andExpect(content().string(containsString("yttrium-aluminum-garnet laser capsulotomy of lens")))
                .andExpect(content().string(containsString("hypertensive heart and renal disease with both (congestive) heart failure and renal failure")))
                .andExpect(content().string(containsString("fetal or neonatal effect of delivery by vacuum extractor")))

                .andExpect(content().string(containsString("insertion of new or replacement of permanent pacemaker with transvenous electrode(s) ventricular"))
                );
    }

    @Test
    public void searchException() throws Exception {
        mvc.perform(MockMvcRequestBuilders.post("/").content("     ").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(content().string(equalTo("[\"" + SearchController.SEARCH_TXT_VALIDATOR_ERROR + "\"]")));
    }
}
