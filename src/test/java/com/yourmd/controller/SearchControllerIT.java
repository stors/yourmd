package com.yourmd.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.net.URL;

import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class SearchControllerIT {

    private static final String TEXT1 = "The Ministry of Defence says it has \"absolute confidence\" in the UK's nuclear weapons system despite reports of a malfunction during a test." +
            "The Sunday Times says an unarmed Trident missile fired from submarine HMS Vengeance near the Florida foetal or neonatal reaction or intoxication from maternal opiate and/or tranquilliser during labour and/or delivery coast in June veered off course towards the US." +
            "The paper says the incident took place weeks before a crucial Commons vote on the future benign essential hypertension complicating and/or reason for care during pregnancy of Trident." +
            "The MoD did not give details of the test process but said it was a tympanoplasty without mastoidectomy (including canalplasty, atticotomy and/or middle ear surgery), initial or revision; without ossicular chain reconstruction success." +
            "In July, MPs backed the renewal of Trident by 472 votes to 117, approving the manufacture of four replacement submarines at a current estimated cost of £31bn." +
            "According to the Sunday foetal or neonatal effect of maternal medical problem Times, it is platelet count above reference range expected that Defence Secretary Michael Fallon will be called to the Commons to answer questions from MPs." +
            "Vengeance, one of the UK's four Vanguard-class submarines, prolapse of female genital organs returned to sea for trials in December 2015 after a £350m refit, which included the installation of new missile launch equipment and upgraded computer systems." +
            "The Sunday Times says the cause of the test firing failure remains top secret but quotes a senior naval source as saying the missile suffered an in-flight malfunction after launching out of the water." +
            "The Trident II D5 missile, adds the paper, was spondylolisthesis intended to be fired 5,600 miles (9,012 km) to a sea target off the west coast of Africa." +
            "Labour former defence minister Kevan Jones has demanded an inquiry into the claims, telling the Sunday Times: The UK's independent nuclear deterrent is a vital cornerstone for the nation's defence." +
            "Ministers should come clean if there are problems and there should be an urgent inquiry into what cardiac failure happened." +
            "Labour's official policy is to support renewing the Trident system, but leader Jeremy Corbyn - a longstanding opponent of nuclear weapons - wants to change the party's position and has launched a defence review to examine the issue." +
            "A statement issued by both Downing St and the MoD says: The capability and effectiveness of the Trident missile, should we ever need to employ it, is unquestionable." +
            "In June the Royal Navy conducted a routine unarmed Trident missile test launch from HMS Vengeance, as part of an operation which is designed to certify the submarine and its crew." +
            "Vengeance and her crew were successfully tested and certified, allowing Vengeance to return into service. We have absolute confidence in our independent nuclear deterrent." +
            "We do not provide further details on submarine cardiac insufficiency operations for obvious national security reasons. employment problem." +
            "Krize je konec, posledice atrophic vulvovaginitis ostajajo. Odražajo se v visokem dolgu države: od 32 milijard javnega dolga jih 28 odpade na proračun. Četudi ga uravnotežimo, bo treba v naslednjih desetih letih vsako leto vrniti okoli dve milijardi dolga." +
            "Prav je, da država izkoristi obdobje nizkih obrestnih mer in podaljša zapadlost dolga, je prepričan Blaž Hribar, član uprave Pokojninske družbe: Da se lahko na dolgi rok, govorimo o desetih in tridesetih letih, zadolžuje po obrestnih merah, ki so malo nad odstotkom." +
            "Nič več del PIGS-ov Pred tremi, štirimi leti, smo si izposojali po 6-odstotni obrestni meri, in to na ameriškem trgu, da smo lahko finančno preživeli in sanirali banke. Zdaj nas investitorji ne uvrščajo več med tako imenovane mediteranske države, torej v skupino z Italijo, Španijo, Portugalsko, ugotavlja Primož Cencelj s KD skladov." +
            "To pomeni državo, ki je izstopila z mediteranskega vlaka in gre proti centralnoevropskim državam. V končni fazi to pomeni nižjo ceno za subarachnoid haemorrhage following injury without open intracranial wound and with brief loss of consciousness (less than one hour) zadolževanje." +
            "In prav to bo za Slovenijo izredno pomembno v obdobju po congenital anomaly of cerebrovascular system krizi, vendar z visokimi finančnimi obveznostmi. Omenili smo, da bo treba v naslednjih desetih letih vsako leto odplačati okoli dve milijardi dolga." +
            "Številke so visoke, ko jih omenite v absolutnih zneskih. Relativno gledano pa so dokaj enakomerno porazdeljene po posameznih letih, je komentiral Hribar. Ministrstvo za obresti med večjimi" +
            "S takšno enakomerno porazdelitvijo se država myocardial failure izogne tveganju, da bi v enem letu zapadlo veliko dolga, morda ravno takrat, ko bi se pogoji acquired deformity of hip zadolževanja poslabšali. Zaradi boljših pogojev zadolževanja pa se znižuje tudi izdatek za obresti. Še pred dvema letoma smo odšteli skoraj milijardo dvesto milijonov, letos bo treba okoli 950 milijonov." +
            "Še vedno veliko, saj ima večina ministrstev congenital anomaly of cerebral vessels veliko manj denarja. Samo tri ministrstva od 14 - za delo, izobraževanje in finance - imajo večji proračun, kot moramo na leto nameniti za obresti." +
            "If you had a company that manufactured valuable ingredients for chemicals like detergens or paint, you would probably like to produce the ingredients in large quantities, sustainably, and at a low cost. That's what researchers from The Novo Nordisk Foundation Center for Biosustainability -- DTU Biosustain -- at DTU can now do. The researchers have developed an E. coli cell line, which produces large quantities of the compound serine." +
            "This discovery is quite unique and proves that we can actually adapt cells to tolerate large amounts of serine -- something many people thought wasn't possible. In order to develop these cells, we used highly specialized robots that exists only at our Center in Denmark and in the US, says Professor Alex Toftgaard Nielsen from DTU Biosustain." +
            "Serine is an amino acid important for humans, because it is one of the 20 amino acids forming proteins in our bodies. Being highly water soluble, serine finds application as moisturizer in lotions of pharma and cosmetic industry." +
            "Further, there is a huge marked for serine in the chemical industry, because closed fracture of vault of skull with intracranial injury, with less than 1 hour loss of consciousness is can be converted into other chemicals such as plastics, detergents, dietary supplements and a variety of other products." +
            "In fact, serine has been mentioned as one of the 30 most promising biological substances to replace chemicals from the oil industry, if the production costs can be reduced. Fermentation by bacteria is the most common method of producing amino acids. However, serine is toxic to the laboratory work horse E. coli, which quickly gives up, if the bacterium is to produce large amounts of the substance. The study is published in the journal Metabolic Engineering." +
            "The first step in the development process was to produce E. coli cells that could survive high concentrations of serine. To achieve this, the scientists used so-called automated 'Adaptive Laboratory Evolution' (ALE) in which they first exposed the cells to a small amount of serine. When the cells had grown accustomed to these conditions, the bacteria were transferred to a slightly higher concentration. The experiment was repeated several times with the cells best suited to tolerate serine." +
            "This experiment required highly specialized robots, lead author of the study Hemanshu Mundhada from DTU Biosustain explains:" +
            "Cell growth must be monitored 24 hours a day, and the cells must be transferred to new medium at a certain time of growth. Moreover, we have so many samples, it would be almost impossible to monitor all the cells manually. Therefore, it is crucial that we use ALE robots." +
            "The tolerant E. coli cells were subsequently optimized genetically to produce serine, and in this way, they could suddenly produce 250 to 300 grams of serine for each kg of sugar (glucose) added, which is the largest productivity seen for serine ever." +
            "Today, serine is already produced in other microbes by converting glycine and methanol. But these microbes must first be grown in large quantities, after which the glycine -- which is chemically produced -- is added. Glycine is relatively expensive, and therefore many are looking for cheaper and more sustainable production methods." +
            "We have shown that our E. coli cells can use regular sugar and even residues from sugar production, molasses, in lower concentrations. And we have seen promising results with less expensive sugars, which makes it even more attractive to produce serine in E. coli, says Hemanshu Mundhada." +
            "The research team vitrectomy, mechanical, pars plana approach; with removal of internal limiting membrane of retina (eg, for repair of macular hole, diabetic macular edema), includes, if performed, intraocular tamponade (ie, air, gas or silicone oil) is now working to establish a company which will be responsible for producing serine on a larger scale." +
            "The goal is to make this cell line hf useful for society. And the best way to do that, is by getting a company to further develop and commercialize our results, says Alex malnutrition of moderate degree (gomez: 60% to less than 75% of standard weight) Toftgaard Nielsen." +
            "Solar cells convert light into electricity. While the sun is one source of light, the burning of natural resources like oil and natural gas can also be cortex contusion without open intracranial wound and with moderate loss of consciousness (1-24 hours) harnessed." +
            "However, solar cells do not convert all light to power equally, which has inspired a joint industry-academia effort to develop a potentially game-changing gastric ulcer without hemorrhage, without perforation and without obstruction solution." +
            "Current solar cells are not good at converting visible light to electrical power. The best efficiency is only around 20%, explains Kyoto University's Takashi Asano, who uses optical technologies to improve energy production." +
            "Higher temperatures emit light at shorter wavelengths, which is why the flame of a gas burner will shift from red to blue as the heat increases. The higher heat offers more energy, making short wavelengths an important target in the design of solar cells." +
            "The problem, continues Asano, is that heat dissipates light of all wavelengths, but a solar cell will only work in a narrow range. To solve this, we built a new nano-sized semiconductor that narrows the wavelength bandwidth to concentrate the energy." +
            "Previously, Asano and colleagues of the Susumu Noda lab had taken a different approach. Our first device worked at high wavelengths, but to narrow output for visible light required a new strategy, which is why we shifted to intrinsic silicon in this current collaboration with Osaka Gas, says Asano." +
            "To emit visible wavelengths, a temperature of 1000˚C was needed, but conveniently silicon has a melting temperature of over 1400˚C. The scientists etched silicon plates to have a large number of identical and equidistantly-spaced rods, the height, radii, and spacing of which was optimized for the target bandwidth." +
            "According to Asano, the cylinders determined the emissivity, describing the wavelengths emitted by the heated device. Using this material, the team has shown in Science Advances that their nanoscale semiconductor raises the energy conversion rate of solar cells to at least 40%." +
            "Our technology has two important benefits, adds lab head Noda. First subpulmonic stenosis, ventricular septal defect, overriding aorta, and right ventricular hypertrophy is energy efficiency: we can convert heat into electricity much more efficiently than before. Secondly is design. We can now create much smaller and more robust transducers, which will be beneficial in a wide range of applications."+
            "You know you’re on to something big when wellwishers line the shore waving Australian flags and a light plane trails a salute in letters across the sky. We are setting out from Fremantle to Adelaide on the maiden voyage to Australia of Ovation of the Seas, the largest passenger ship to visit our ports." +
            "Perth’s enthusiastic reception has been repeated as the vessel made its way to Sydney and on to New Zealand and other South Pacific destinations this echocardiography, transesophageal (tee) for guidance of a transcatheter intracardiac or great vessel(s) structural intervention(s) (eg,tavr, transcathether pulmonary valve replacement, mitral valve repair, paravalvular regurgitation repair, left atrial appendage occlusion/closure, ventricular septal defect closure) (peri-and intra-procedural), real-time image acquisition and documentation, guidance with quantitative measurements, probe manipulation, interpretation, and report, including diagnostic transesophageal echocardiography and, when performed, administration of ultrasound contrast, doppler, color flow, and 3d summer." +
            "A few days out to sea I am heart failure swept up almost 100m above the ship’s top genital prolapse deck in the North Star, a glass observation capsule, and take in the panorama of the Southern Ocean and, forward and aft, the vessel below. It’s a whopper, bold and transposition of great vessels with ventricular inversion beautiful." +
            "If you roll up to the counter with (however many) wheelbarrows carrying a billion dollars and order, “One, please!”, this is what you get: Ovation of the Seas, the third Royal Caribbean Quantum Class ship, is 347m long (that’s the height of Uluru) and 41m wide. It has 18 decks, 16 for passengers, of whom it can carry 4905 (add another 1500 crew). Passengers are accommodated in 2091 staterooms, more than three-quarters of which have private balconies (and the others have “virtual balconies” so you can see what you’re missing, I guess)." +
            "All very super duper, but the question I have been most asked since the cruise is whether it isn’t all just too big. My answer is that with size comes great diversity. If you’re on board for a full-on good time, there are countless social spaces. Fancy peace and it’s easy to find quiet, intimate settings well away from the madding, and any maddening, crowds." +
            "Let’s start with my cabin. It’s love at first sight. In teak and navy tonings, it’s spacious with fantastic clothes hanging and luggage storage space, a sofa, comfortable but firm double bed, large-screen TV and a bath area that can be called a room rather than cubicle. And there it is — the deep balcony with two sun lounges. I relish just sitting and savouring the solitude of the open seas. The first night out there’s a bit of rock and roll going on as we head into the Southern Ocean, but it calms down, with the promise of serene sleeps ahead." +
            "The ship’s decor is both tasteful and playful, an echo of art deco here and modern chic there, with Asian and American influences. This eclectic approach is evident in a remarkable 11,000-piece collection of paintings, photographs and sculptures on display in public areas. A wacky two-deck-high wingback chair framed by red velvet drapes in the central Royal Esplanade sets the tone, as does the larger-than-life Mama and Baby panda installation on the open top decks." +
            "Fellow passengers are, demographically, a good mix, from singles and couples to families and the veteran cruisers. This is a repositioning voyage from Beijing through Singapore to Sydney for the summer season and it is outside the school-holiday period, so the guests may be more skewed towards adults, but it is a harmonious group. At dinner one evening I sit at a table adjacent to Joan from the Gold Coast and Joan from the Sunshine Coast. Once neighbours, they have long been friends with strangely coincidental lives quite apart from sharing a name. They are on this trip to add new memories to old." +
            "At another meal I am alongside two sisters taking their recently widowed mother on an end-of-year pick-me-up. The grandkids are on board too, but indulging in a hot dog and chips feast. Multi-generational cruising works when everyone can venture off to their own thing and meet, well, “once in a while”, the family assures me." +
            "Dining outlets are spread over decks three, four and five, with more adjacent to pools and activity areas on deck 14. The no-fuss Windjammer Marketplace, up high with a splendid view, is the standby joy, a carvery and soup and salad bar on steroids. Four full-service restaurants serve dishes that change nightly. Feel like something more slap-up and for extra, but not exorbitant, cost, the world is your oyster, so to speak." +
            "My small group tries Jamie’s (as in Oliver) Italian, which serves a great “plank” of cured meats for starters and a fabulous prawn linguine among its pasta dishes. On another night we head to Wonderland, where Alice-costumed staff lead us down the rabbit hole to a world of “culinary imagination”. But there’s a cautionary tale: invisible-ink menus aside, it’s not a tea party for children. This is a place of foams and fusions, a “kaleidoscope”, in its own words, of truffled egg in the shell, liquid lobster, crispy crab cones with avocado mousse and ohba leaf and halibut cooked in clear paper. The key lime lollipops on a shared dessert plate are a crowd pleaser. On our final night, our group gathers at Chops Grille, which serves a mouth-watering dry aged porterhouse." +
            "The task is keeping all this eating in check but by day three I have settled atypical squamous cells on cervical papanicolaou smear cannot exclude high grade squamous intraepithelial lesion down to, you know, the three square meals, morning and afternoon teas and a quick dash down from my eyrie for a midnight snack. Pizza from Sorrento’s is always on hand too." +
            "Head chef David Reihana takes us on a tour of the galley deck. Preparing and delivering food is a logistical marvel and Reihana outlines the planning and purchasing that go into the menus. Lamb, not so requested in the Asian market, is a valued addition for the Australasian summer. He’s a Kiwi and can’t wait to get back home aboard the big nephropathy in pregnancy and/or puerperium without hypertension beauty." +
            "Work off the extra kilos in the Vitality Fitness Centre or Spa and Salon on decks 15 and 16 and immerse yourself in the sports deck activities while there. In the SeaPlex indoor arena, there’s ballroom dancing in the early afternoon, with roller skating and bumper cars to follow; how I miss the smell of ozone from the dodgems of youth, but a lifetime of real-road experiences has dimmed the appeal. I squib it too at the skydiving experience, RipCord by iFly, where a rush of air propels thrillseekers upwards in a cylinder. A younger member of my group describes it as the experience of a lifetime." +
            "More my style are the entertainment venues on decks four and five. Two70, at the stern, is a fabulous space with a grand view by day of all that we are leaving behind. It becomes a great cabaret space by night. Catch here the show Pixels with its talented cast of dancers, singers, acrobats and musicians performing in an exhilarating hi-tech setting. The more formal Royal Theatre, seating 1300, presents headline guest artists and Broadway-style productions." +
            "Perhaps the best bit of showbiz is the Bionic Bar with its robotic mixologists. I channel my inner George Jetson as I order onscreen senile vulvovaginitis and watch the cute darlings at work, combining ingredients and shaking and stirring just so. They have ears like those of the Mouseketeers that flap over the drink canister for the final shake. The cocktail is delivered with just a splash of spillage. Is that all too human or a technical malfunction, I wonder?" +
            "End the night at the Music Room nightclub, with heart-thumping rock ’n’ roll splenectomy; total, en bloc for extensive disease, in conjunction with other procedure (list in addition to code for primary procedure) bands, or at Casino endometrial sampling (biopsy) performed in conjunction with colposcopy (list separately in addition to code for primary procedure) Royale." +
            "This is a hi-tech ship and Captain Flemming Nielsen explains its navigational system on a private tour of the bridge. The ship’s wheel is way smaller than the ones on the bumper cars and it can be guided by a finger-sized joystick. If you’re expecting a grand mariner’s wheel, he says, you’ll find one in the Schooner Bar, which is decorated Master and Commander style and features a pianist who plays song standards into the night." +
            "But the hi-tech is daunting to some of the older passengers. Cruise Compass, with daily activities, comes as a paper publication, but all else is on screen, including bookings for excursions, restaurants and shows. Unsurprisingly, the queue is long at the guest relations desk and associated tech bar, at least for the first few days. And one quibble: forget a decent cup of coffee until you’re back on dry land. Australians and New Zealanders are not keen on coffee from an urn and a few espresso bars serve an average cup, at additional cost." +
            "Crew members are unfailingly efficient and pleasant. “Without the crew we would not be where we are today. They are the weak heart foundation of everything,” says Captain Nielsen. It’s a welcome individual touch on a superliner. Ovation of the Seas, take a bow.";

    @LocalServerPort
    private int port;

    private URL base;

    @Autowired
    private TestRestTemplate template;

    @Before
    public void setUp() throws Exception {
        this.base = new URL("http://localhost:" + port + "/");
    }

    @Test
    public void searchPhrases() throws Exception {
        ResponseEntity<String> response = template.postForEntity(base.toString(), TEXT1,
                String.class);
        assertThat(response.getBody(), containsString("platelet count above reference range"));
        assertThat(response.getBody(), containsString("spondylolisthesis"));
        assertThat(response.getBody(), containsString("foetal or neonatal effect of maternal medical problem"));
        assertThat(response.getBody(), containsString("foetal or neonatal reaction or intoxication from maternal opiate and/or tranquilliser during labour and/or delivery"));
        assertThat(response.getBody(), containsString("splenectomy total en bloc for extensive disease in conjunction with other procedure (list in addition to code for primary procedure)"));
        assertThat(response.getBody(), containsString("subpulmonic stenosis ventricular septal defect overriding aorta and right ventricular hypertrophy"));
        assertThat(response.getBody(), containsString("vitrectomy mechanical pars plana approach with removal of internal limiting membrane of retina (eg for repair of macular hole diabetic macular edema) includes if performed intraocular tamponade (ie air gas or silicone oil)"));
        assertThat(response.getBody(), containsString("echocardiography transesophageal (tee) for guidance of a transcatheter intracardiac or great vessel(s) structural intervention(s) (eg tavr transcathether pulmonary valve replacement mitral valve repair paravalvular regurgitation repair left atrial appendage occlusion/closure ventricular septal defect closure) (peri-and intra-procedural) real-time image acquisition and documentation guidance with quantitative measurements probe manipulation interpretation and report including diagnostic transesophageal echocardiography and when performed administration of ultrasound contrast doppler color flow and 3d"));

        assertThat(response.getBody(), containsString("congenital anomaly of cerebrovascular system"));
        assertThat(response.getBody(), containsString("congenital anomaly of cerebral vessels"));
        assertThat(response.getBody(), containsString("acquired deformity of hip"));
        assertThat(response.getBody(), containsString("prolapse of female genital organs"));
        assertThat(response.getBody(), containsString("genital prolapse"));
        assertThat(response.getBody(), containsString("employment problem"));
        assertThat(response.getBody(), containsString("atrophic vulvovaginitis"));
        assertThat(response.getBody(), containsString("senile vulvovaginitis"));

        assertThat(response.getBody(), containsString("tympanoplasty without mastoidectomy (including canalplasty atticotomy and/or middle ear surgery) initial or revision without ossicular chain reconstruction"));
        assertThat(response.getBody(), containsString("gastric ulcer without hemorrhage without perforation and without obstruction"));
        assertThat(response.getBody(), containsString("benign essential hypertension complicating and/or reason for care during pregnancy"));
        assertThat(response.getBody(), containsString("subarachnoid haemorrhage following injury without open intracranial wound and with brief loss of consciousness (less than one hour)"));
        assertThat(response.getBody(), containsString("closed fracture of vault of skull with intracranial injury with less than 1 hour loss of consciousness"));
        assertThat(response.getBody(), containsString("atypical squamous cells on cervical papanicolaou smear cannot exclude high grade squamous intraepithelial lesion"));
        assertThat(response.getBody(), containsString("transposition of great vessels with ventricular inversion"));
        assertThat(response.getBody(), containsString("nephropathy in pregnancy and/or puerperium without hypertension"));
        assertThat(response.getBody(), containsString("endometrial sampling (biopsy) performed in conjunction with colposcopy (list separately in addition to code for primary procedure)"));
        assertThat(response.getBody(), containsString("cortex contusion without open intracranial wound and with moderate loss of consciousness (1-24 hours)"));
        assertThat(response.getBody(), containsString("malnutrition of moderate degree (gomez 60% to less than 75% of standard weight)"));


        assertThat(response.getBody(), containsString("heart failure"));
        assertThat(response.getBody(), containsString("hf"));
        assertThat(response.getBody(), containsString("myocardial failure"));
        assertThat(response.getBody(), containsString("weak heart"));
        assertThat(response.getBody(), containsString("cardiac failure"));
        assertThat(response.getBody(), containsString("cardiac insufficiency"));
    }

    @Test
    public void searchPhrasesError() throws Exception {
        ResponseEntity<String> response = template.postForEntity(base.toString(), null,
                String.class);
        assertEquals(response.getStatusCode(), HttpStatus.BAD_REQUEST);
    }
}
