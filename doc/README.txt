How algorithm works:

input text: as b cd ef gy h ij kl mn op
stored phrase: cd ef gy

First we store unique words(key), value is large number ID.
Also we store window size for first word (wordMap):
key -> cd, value -> 1, window size = 3
key -> ef, value -> 2, window size = 0
key -> gy, value -> 3, window size = 0

We have then another map which construct phrase from previous map (connectMap):
key -> 123, value -> cd ef gy

We can unique identify phrase through numbers -> 1(cd)2(ef)3(gy)

as - no key found, b - no key found ()

When we look now our input text, when we come to word cd we get value:
value -> 1, window size 3

Now we have window to check from (cd ef gy) - look ahead:
wordMap: cd, value -> 1
connectMap: exist key with 1 - no

wordMap: ef, value -> 2
connectMap: exist key with 12 - we combine previous with current number - no

wordMap: gy, value -> 3
connectMap: exist key with 123 - yes value is cd ef gy. We have a match!


Time complexity for this algorithm is O(n).
Search consist - break down of time complexity:
- Break down of input string to array of words (split - O(k)) plus convert to list - O(k) - we have O(2k).
    (this can be further optimized for O(k) time complexity - depends of input string length if we need this optimization)
- Main search (input string) where every word is checked against wordMap lookup -> O(n) plus O(1) lookup - we have O(n)
- Ahead lookUp of sub lists -> search sub list for phrases in storage - search sub list O(m) plus lookup connectMap O(1)

We have:
Break down input string (k), search first word (n), search sub list(m)
O(2k) + O(n) * O(m)
The whole time complexity is O(n^2) - the worst case where input string is equals one phrase in storage (this is only for small sets).
Because in most cases O(n) > O(m) (n is much larger than m) we can say that the whole time complexity of searchis in O(n) - liner search with look ahead function.

Search is especially effective in small and large input text scenarios.
Also usage of Hazelcast in memory noSQL database for efficient lookup which is in time complexity O(1) + network - if we have distributed version.

The project consist of main break down:

- configuration (configures storage and other stuff)
- controller
- service (main logic part)
- data (where we handle storage based methods)
- validator (validate all input messages)
- helper (for any helper classes and methods)