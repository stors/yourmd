# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Task for YourMD.
* Takes as input a string parameter and returns a list of matched phrases from a dictionary list.
* Task is created using Spring Boot.

### How do I get set up? ###
Build (with tests):

```
#!script

gradle build
```


Build (without tests):

```
#!script

gradle build -x test
```

Execute spring boot application:

```
#!script

gradle build && java -jar build/libs/gs-spring-boot-0.1.0.jar
```

Debugging:

```
#!script

gradle bootRun --debug-jvm
```

### Documentation ###

Documentation description of task can be found on:
```
#!script

/doc
```